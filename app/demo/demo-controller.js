module.exports = function ($scope, Shoe, DemoData, $timeout, $interval, $state) {

  $scope.Strings = {
    intro0: 'Introducing Provably Fair Decks of Cards&nbsp;Using CryptoLock™ Over a Peer-to-Peer Network.',
    cardsDealt: 'Two cards are dealt to each player.'
  };

  $scope.first_name = DemoData.first_name;
  $scope.fav_color = DemoData.fav_color;
  $scope.generated_hash = DemoData.generated_hash;
  $scope.random_hash = DemoData.random_hash = Shoe.random();
  $scope.step3_hidenextbtn = true;
  $scope.player1_hash = DemoData.player1_hash;

  $scope.boxClass = false;
  $scope.cardordertitles = ["TOP", "SECOND", "THIRD", "FOURTH"];
  $scope.deckplayer1 = [];
  $scope.saltsplayer1 = [];
  $scope.hashesplayer1 = [];
  for (var i = 0; i < 4; i++) {
    $scope.deckplayer1[i] = Shoe.random();
  }
  $scope.currentHashingCard = 0;

  $scope.step1TextsChanged = function() {
    $scope.is_generated_hash_shown = false;
    $scope.is_create_hash_shown = $scope.first_name && $scope.fav_color;
  }

  $scope.step1CreateHash = function () {
    //var Shoe = require('provably-fair');
    DemoData.first_name = $scope.first_name;
    DemoData.fav_color = $scope.fav_color;
    DemoData.generated_hash = $scope.generated_hash = Shoe.hash($scope.first_name + '' + $scope.fav_color);
    $scope.is_generated_hash_shown = true;
  }

  $scope.player1Hash = function () {
    //DemoData.player1_salt = $scope.player1_salt;
    //DemoData.player1_hash = $scope.player1_hash = Shoe.hash($scope.random_hash+''+$scope.player1_salt);
    //$scope.step3_hidenextbtn = false;
    $scope.saltsplayer1[$scope.currentHashingCard] = $scope.player1_salt;
    $scope.hashesplayer1[$scope.currentHashingCard] =
      Shoe.hash($scope.deckplayer1[$scope.currentHashingCard] +
        '' + $scope.saltsplayer1[$scope.currentHashingCard]);
  };

  $scope.putCardToBottom = function () {
    var el3 = $('.step-reverse3');
    var el2 = $('.step-reverse2');
    var el1 = $('.step-reverse1');
    var el0 = $('.step-reverse0');
    el3.removeClass().addClass('shuffle-card to-top');
    $timeout(function () {
      el3.removeClass().addClass('shuffle-card step-reverse0 scaling0');
      el2.removeClass().addClass('shuffle-card step-reverse3');
      el1.removeClass().addClass('shuffle-card step-reverse2 scaling2');
      el0.removeClass().addClass('shuffle-card step-reverse1 scaling1');
    }, $scope.transition_time);
  };

  $scope.player1HashNext = function () {
    if ($scope.currentHashingCard == 3) {
      $scope.step3_hidenextbtn = false;
    } else {
      $scope.putCardToBottom();
      $scope.currentHashingCard++;
      $scope.player1_salt = "";
    }
  };

  $scope.player2Hash = function () {
    DemoData.player2_salt = $scope.player2_salt;
    DemoData.player2_hash = $scope.player2_hash = Shoe.hash(DemoData.player1_hash + '' + $scope.player2_salt);
  };

  $scope.transition_time = 200;
  $scope.number_of_cards = 4;

  $scope.shuffle = function () {
    var iterations = 2;
    if (!$scope.animating) {
      $scope.animating = true;
      //repeat shuffle animation
      $interval(function () {
        $scope.setAsStepClass = false;
        $scope.animationClass = true;
        $timeout(function () {
          $scope.animationClass = false;
        }, $scope.transition_time);
      }, $scope.transition_time * 2, iterations);
      //set steps after animation ended
      $timeout(function () {
        $scope.setAsStepClass = true;
        $scope.animating = false;
      }, $scope.transition_time * 2 * (iterations + 1));
    }
  };

  $scope.dealing = function () {
    $scope.setAsStepClass = true;
    $scope.animating = true;
    var animation_time = $scope.transition_time * $scope.number_of_cards;
    var animation_delay = 500;
    //start dealing animation after n of seconds
    $timeout(function () {
      $scope.dealingClass = true;
    }, animation_delay);
    $timeout(function () {
      $scope.animating = false;
    }, animation_time + animation_delay);
  };

  $scope.appearing = function () {
    $scope.animating = true;
    var appearing_time = 800;
    $timeout(function () {
      $scope.appearClass = true;
    });
    $timeout(function () {
      $scope.animating = false;
    }, $scope.number_of_cards * appearing_time);
  };


  $scope.firstCard = function () {
    $scope.setReverseStepClass = true;
    $timeout(function () {
      $scope.setScalingClass = true;
    })
  };

  //console.log($state.current.name);
  switch ($state.current.name) {
    case 'demo.step1':
      $scope.shuffle();
      break;
    case 'demo.intro':
      $scope.appearing();
      break;
    case 'demo.dealing':
      $scope.dealing();
      break;
    case 'demo.step4':
      $scope.firstCard();
      break;
  }
};
