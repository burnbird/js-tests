// require all of the core libraries
require('../public/lib/angular/angular.min');
require('../public/lib/angular-ui-router/release/angular-ui-router.min');

// pull in the modules we are going to need (controllers, services, whatever)
var Shoe = require('provably-fair');

var demoController = require('./demo/demo-controller.js');

// module up
var app = angular.module('bitboss', ['ui.router']);
// create factories
app.factory('Shoe', Shoe);
app.factory('DemoData', function () {
  return {
    first_name: "",
    fav_color: "",
    generated_hash: "",
    random_hash: "",
    player1_salt: "",
    player1_hash: "",
    player2_salt: "",
    player2_hash: ""
  };
});

// create controllers
app.controller('demoController', ['$scope', 'Shoe', 'DemoData', '$timeout', '$interval', '$state', demoController]);
angular
  .module('bitboss')
  .config(routeConfig);

routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

function routeConfig($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('demo', {
      abstract: true,
      url: '/',
      template: '<ui-view/>'
    })
    .state('demo.intro', {
      url: '',
      templateUrl: '/templates/demo/0intro.html',
      controller: 'demoController',
    })
    .state('demo.hashing', {
      url: 'hashing',
      templateUrl: '/templates/demo/1hashing.html',
      controller: 'demoController',
    })
    .state('demo.explain', {
      url: 'explain',
      templateUrl: '/templates/demo/2explain.html',
      controller: 'demoController',
    })
    .state('demo.gamestart', {
      url: 'gamestart',
      templateUrl: '/templates/demo/3gamestart.html',
      controller: 'demoController',
    })
    .state('demo.step1', {
      url: 'step1',
      templateUrl: '/templates/demo/4step1.html',
      controller: 'demoController'
    })
    .state('demo.step3', {
      url: 'step3',
      templateUrl: '/templates/demo/5step3.html',
      controller: 'demoController',
    })
    .state('demo.step4', {
      url: 'step4',
      templateUrl: '/templates/demo/5step4.html',
      controller: 'demoController',
    })
    .state('demo.step5', {
      url: 'step5',
      templateUrl: '/templates/demo/6step5.html',
      controller: 'demoController',
    })
    .state('demo.ready', {
      url: 'ready',
      templateUrl: '/templates/demo/7ready.html',
      controller: 'demoController',
    })
    .state('demo.dealing', {
      url: 'dealing',
      templateUrl: '/templates/demo/demo-8.html',
      controller: 'demoController',
    })
    .state('demo.dealt', {
      url: 'dealt',
      templateUrl: '/templates/demo/demo-9.html',
      controller: 'demoController',
    })
    .state('demo.card1-1', {
      url: 'card1-1',
      templateUrl: '/templates/demo/demo-10.html',
      controller: 'demoController',
    })
    .state('demo.card1-2', {
      url: 'card1-2',
      templateUrl: '/templates/demo/demo-11.html',
      controller: 'demoController',
    })
    .state('demo.card1-3', {
      url: 'card1-3',
      templateUrl: '/templates/demo/demo-12a.html',
      controller: 'demoController',
    })
    .state('demo.card1-4', {
      url: 'card1-4',
      templateUrl: '/templates/demo/demo-12b.html',
      controller: 'demoController',
    })
    .state('demo.card1-5', {
      url: 'card1-5',
      templateUrl: '/templates/demo/demo-13.html',
      controller: 'demoController',
    })
    .state('demo.card2-1', {
      url: 'card2-1',
      templateUrl: '/templates/demo/demo-14.html',
      controller: 'demoController',
    })
    .state('demo.card2-2', {
      url: 'card2-2',
      templateUrl: '/templates/demo/demo-15.html',
      controller: 'demoController',
    })
    .state('demo.card2-3', {
      url: 'card2-3',
      templateUrl: '/templates/demo/demo-16.html',
      controller: 'demoController',
    })
    .state('demo.card2-4', {
      url: 'card2-4',
      templateUrl: '/templates/demo/demo-17.html',
      controller: 'demoController',
    })
    .state('demo.card2-5', {
      url: 'card2-5',
      templateUrl: '/templates/demo/demo-18.html',
      controller: 'demoController',
    })
    .state('demo.newpage', {
      url: 'newpage',
      templateUrl: '/templates/demo/99new.html',
      controller: 'demoController',
    })
}
