var gulp = require('gulp');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
gulpLoadPlugins = require('gulp-load-plugins');

var plugins = gulpLoadPlugins();
var browserify = require('browserify');
var template = require('gulp-template');

var paths = {
  sass: ['./app/common/stylesheets/**/*.scss'],
  coffee: ['./app/**/*.js'],
  templates: ['./app/**/*.html']
};

gulp.task('default', ['templates', 'browserify', 'sass']);

gulp.task('templates', function() {
  gulp.src(paths.templates)
    .pipe(template({}))
    .pipe(gulp.dest('./public/templates'));
});

gulp.task('browserify', function() {
    return browserify({
      entries: ['./app/index.js'],
    })
    .bundle()
    //Pass desired output filename to vinyl-source-stream
    .pipe(source('application.min.js'))
    // Start piping stream to tasks!
    .pipe(gulp.dest('./public/dist'));
});

gulp.task('sass', function(done) {
  gulp.src('./app/common/stylesheets/application.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./public/dist'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./public/dist'))
    .pipe(plugins.livereload())
    .on('end', done);
});


gulp.task('watch', function() {
  plugins.livereload.listen();
  gulp.watch(paths.coffee, ['browserify']).on('change', plugins.livereload.changed);
  gulp.watch(paths.templates, ['templates']).on('change', plugins.livereload.changed);
  gulp.watch(paths.sass, ['sass'])
  // Start livereload
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
