in order of the test do fork this repo and then share your repo with me.

# task 1

how this code can be refactored and improved?
do some refactoring and write what you've changed and why 


# task 2

add new page (state), name it task-2 or whatever else, create controller, view etc

install this lib https://www.npmjs.com/package/ecc-tools 

add to this page:

* text input (name = plaintext)
* textarea (name = cipher)
* button "init" which inits key pair (output it anywhere you want, even by console.log)
* button "encode text" which will encrypt entered text (using ecc-tool), convert the cipher to JSON and output it into "cipher" textarea
* button "decode text" which will decrypt entered cipher to the plain string and output it to the "plaintext" input (or alert with an error if cipher can't be decoded).




# short details about first run

### to build the app, execute the following commands:
    npm install
    bower install
    gulp

### to run the app using node-static
    npm install -g node-static
    npm start

#### if you are in Ubuntu and use nvm
probably you will get the problem with `npm install` and node-sass package
in this case try install package node-sass globally

    npm install -g node-sass

and then do fresh install

    cd /project/folder
    rm -rf ./node_modules
    npm install

if it doesn't help just run `npm install` again and then you must to do `npm install` and `gulp` inside.
the installed provably-fair library

    npm install
    cd node_modules/provably_fair
    npm install
    gulp